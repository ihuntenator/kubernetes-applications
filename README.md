# Kubernetes Applictions

Creating objects and applications from manifest files.

File Manifest | Description
--------------|------------
testing.yml | create namespace testing
nginx_simple_deployment.yaml | create nginx application
emitter-pod.yml | create noisey pod



## Use as follows:

```
kubectl apply -f  https://bitbucket.org/ihuntenator/kubernetes-applications/raw/master/nginx_simple_deployment.yaml
```


## hello-app

Cant pull from gcr.io so grab source build image and push to docker.io.

https://github.com/GoogleCloudPlatform/kubernetes-engine-samples.git

```
$ cd kubernetes-engine-samples/hello-app
$ docker build -t hello-app .
$ docker tag hello-app ihuntenator/hello-app:1.0
$ docker push ihuntenator/hello-app:1.0
```

